package it.cnr.isti.hiis.pepperhelloword;

public enum BasicEmotion {
    UNKNOWN,
    NEUTRAL,
    CONTENT,
    JOYFUL,
    SAD,
    ANGRY
}

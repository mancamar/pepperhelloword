package it.cnr.isti.hiis.pepperhelloword;

public interface OnBasicEmotionChangedListener {
    void onBasicEmotionChanged(BasicEmotion basicEmotion);
}

package it.cnr.isti.hiis.pepperhelloword;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.aldebaran.qi.Future;
import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.QiSDK;
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks;
import com.aldebaran.qi.sdk.builder.AnimateBuilder;
import com.aldebaran.qi.sdk.builder.AnimationBuilder;
import com.aldebaran.qi.sdk.builder.ChatBuilder;
import com.aldebaran.qi.sdk.builder.ListenBuilder;
import com.aldebaran.qi.sdk.builder.PhraseSetBuilder;
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder;
import com.aldebaran.qi.sdk.builder.SayBuilder;
import com.aldebaran.qi.sdk.builder.TopicBuilder;
import com.aldebaran.qi.sdk.design.activity.RobotActivity;
import com.aldebaran.qi.sdk.object.actuation.Animate;
import com.aldebaran.qi.sdk.object.actuation.Animation;
import com.aldebaran.qi.sdk.object.conversation.Chat;
import com.aldebaran.qi.sdk.object.conversation.Listen;
import com.aldebaran.qi.sdk.object.conversation.ListenResult;
import com.aldebaran.qi.sdk.object.conversation.PhraseSet;
import com.aldebaran.qi.sdk.object.conversation.QiChatExecutor;
import com.aldebaran.qi.sdk.object.conversation.QiChatbot;
import com.aldebaran.qi.sdk.object.conversation.Say;
import com.aldebaran.qi.sdk.object.conversation.Topic;
import com.aldebaran.qi.sdk.object.locale.Language;
import com.aldebaran.qi.sdk.object.locale.Locale;
import com.aldebaran.qi.sdk.object.locale.Region;
import com.aldebaran.qi.sdk.object.touch.Touch;
import com.aldebaran.qi.sdk.object.touch.TouchSensor;
import com.aldebaran.qi.sdk.object.touch.TouchState;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends RobotActivity implements RobotLifecycleCallbacks, OnBasicEmotionChangedListener {

    private TouchSensor headTouchSensor;
    private TouchSensor lHandTouchSensor;
    private QiContext qiContext;
    private BasicEmotionObserver basicEmotionObserver;
    // Store the Chat action.
    private Chat chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        QiSDK.register(this, this);
        basicEmotionObserver = new BasicEmotionObserver();
        basicEmotionObserver.setListener(this);
    }

    @Override
    protected void onDestroy() {
        // Unregister the RobotLifecycleCallbacks for this Activity.
        QiSDK.unregister(this, this);
        basicEmotionObserver.setListener(null);
        basicEmotionObserver = null;
        super.onDestroy();
    }


    @Override
    public void onRobotFocusGained(final QiContext qiContext) {
        // Create a new say action.
        this.qiContext = qiContext;

        Say say = SayBuilder.with(qiContext) // Create the builder with the context.
                .withText("Hello human!") // Set the text to say.
                .build(); // Build the say action.
        // Execute the action.
        say.run();


        Touch touch = qiContext.getTouch();
        headTouchSensor = touch.getSensor("Head/Touch");
        headTouchSensor.addOnStateChangedListener(new TouchSensor.OnStateChangedListener() {
            @Override
            public void onStateChanged(TouchState touchState) {
                Log.i("Head/Touch", "Sensor " + (touchState.getTouched() ? "touched" : "released") + " at " + touchState.getTime());
                Say say = SayBuilder.with(qiContext) // Create the builder with the context.
                        .withText("You touched my head!") // Set the text to say.
                        .build();
                say.run();

                Animation myAnimation = AnimationBuilder.with(qiContext)
                        .withResources(R.raw.animation_test)
                        .build();

                Animate animate = AnimateBuilder.with(qiContext)
                        .withAnimation(myAnimation)
                        .build();

                animate.async().run();
            }
        });
        lHandTouchSensor = touch.getSensor("LHand/Touch");
        lHandTouchSensor.addOnStateChangedListener(new TouchSensor.OnStateChangedListener() {
            @Override
            public void onStateChanged(TouchState touchState) {
                PhraseSet phraseSet = PhraseSetBuilder.with(qiContext)
                        .withTexts("dance", "move")
                        .build();
                Listen listen = ListenBuilder.with(qiContext)
                        .withPhraseSet(phraseSet)
                        //.withLocale(locale)
                        .build();

                ListenResult listenResult = listen.run();
                Say say = SayBuilder.with(qiContext) // Create the builder with the context.
                        .withText("Heard phrase:"+listenResult.getHeardPhrase().getText()) // Set the text to say.
                        .build();
                say.run();
            }
        });
        basicEmotionObserver.startObserving(qiContext);

        Topic topic = TopicBuilder.with(qiContext) // Create the builder using the QiContext.
                .withResource(R.raw.greetings)
                .build(); // Build the topic.

        // Create a new QiChatbot.
        QiChatbot qiChatbot = QiChatbotBuilder.with(qiContext)
                .withTopic(topic)
                .build();
        Map<String, QiChatExecutor> executors = new HashMap<>();

        // Map the executor name from the topic to our qiChatExecutor
        executors.put("myExecutor", new MyQiChatExecutor(qiContext, this));

        // Set the executors to the qiChatbot
        qiChatbot.setExecutors(executors);
        // Create a new Chat action.
        chat = ChatBuilder.with(qiContext)
                .withChatbot(qiChatbot)
                .build();
        // Add an on started listener to the Chat action.
        chat.addOnStartedListener(() -> Log.i("Discussion", "Discussion started."));
        // Run the Chat action asynchronously.
        Future<Void> chatFuture = chat.async().run();
        // Add a lambda to the action execution.
        chatFuture.thenConsume(future -> {
            if (future.hasError()) {
                Log.e("Discussion", "Discussion finished with error.", future.getError());
            } else {

            }
        });
    }

    @Override
    public void onRobotFocusLost() {
        if (headTouchSensor != null) {
            headTouchSensor.removeAllOnStateChangedListeners();
        }
        basicEmotionObserver.stopObserving();
        // Remove on started listeners from the Chat action.
        if (chat != null) {
            chat.removeAllOnStartedListeners();
        }
    }

    @Override
    public void onRobotFocusRefused(String reason) {

    }

    @Override
    public void onBasicEmotionChanged(BasicEmotion basicEmotion) {
        Log.i("EMOTION", "Basic emotion changed: " + basicEmotion);
        Say say = SayBuilder.with(qiContext) // Create the builder with the context.
                .withText("Emotion changed: ".concat(basicEmotion.toString())) // Set the text to say.
                .build(); // Build the say action.
        say.run();
        //TextView textView = (TextView) findViewById(R.id.center_string);
        //textView.setText("Emotion changed: ".concat(basicEmotion.toString()));
    }

    public void cambiapagina(String pagina) {
        if(pagina=="login"){
            System.out.println("DENTRO CAMBIA PAGINA ");
            Intent switchActivityIntent = new Intent(this, SecondActivity.class);
            startActivity(switchActivityIntent);
            //System.out.println("DENTRO CAMBIA PAGINA ");

        }

    }
}


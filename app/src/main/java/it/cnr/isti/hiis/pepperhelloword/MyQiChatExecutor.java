package it.cnr.isti.hiis.pepperhelloword;

import android.content.Intent;
import android.util.Log;

import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.builder.SayBuilder;
import com.aldebaran.qi.sdk.object.conversation.BaseQiChatExecutor;
import com.aldebaran.qi.sdk.object.conversation.Say;

import java.util.List;

class MyQiChatExecutor extends BaseQiChatExecutor {
    private final QiContext qiContext;
    private MainActivity m;

    MyQiChatExecutor(QiContext context, MainActivity m) {
        super(context);
        this.qiContext = context;
        this.m = m;
    }

    @Override
    public void runWith(List<String> params) {
        // This is called when execute is reached in the topic
        Log.i("ChatExecutor", "QiChatExecutor executed");
        m.cambiapagina("login");
    }

    @Override
    public void stop() {
        // This is called when chat is canceled or stopped
        Log.i("ChatExecutor", "QiChatExecutor stopped");
    }


}